$(document).ready(function() {

    $('.slide-top').slick({
        dots: false,
        nav: true
    });
    $(window).scroll(function() {
        var e = $(window).scrollTop();
        if (e > 1) {
            $(".header").addClass("bg");

        } else {
            $(".header").removeClass("bg");
        }
    });
    var active_dates = ["2/11/2020"];
    $("#datepicker").datepicker({
        format: "dd/mm/yyyy",
        language: "vi",
        todayHighlight:	true,
        beforeShowDay: function(date){
            var d = date;
            var curr_date = d.getDate();
            var curr_month = d.getMonth() + 1; //Months are zero based
            var curr_year = d.getFullYear();
            var formattedDate = curr_date + "/" + curr_month + "/" + curr_year
   
            if ($.inArray(formattedDate, active_dates) != -1){
              return {
                 classes: 'activeClass'
              };
            }
         return;
        }
    });
    
    $('#datepicker').on('changeDate', function() {
        $('#my_hidden_input').val(
            $('#datepicker').datepicker('getFormattedDate')
        );
        if($('#datepicker').datepicker('getFormattedDate') == '02/11/2020'){
            $('#listEvent').show();
        }
        else{
            $('#listEvent').hide();
        }
    });
});