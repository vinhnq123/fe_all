;(function() {
  'use strict';


  $(activate);


  function activate() {

    $('.nav-tabs')
      .scrollingTabs({
        bootstrapVersion: 4,  
        enableSwiping: true
      })
      .on('ready.scrtabs', function() {
        $('.tab-content').show();
      });

  }
}());
